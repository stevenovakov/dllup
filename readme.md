# dllup: the new dllu personal website and markup language

This is a personal website aiming for simplicity, ease of maintenance, and support for things like LaTeX math equations.

Tools used:

* [SASS](http://sass-lang.com/)
* [svgtex](https://github.com/agrbin/svgtex)
* [jpegoptim](https://github.com/tjko/jpegoptim)
* [optipng](http://optipng.sourceforge.net/)
